package es.softwarerural.dinobike

import android.app.Application
import es.softwarerural.dinobike.ideas.Ideas
import es.softwarerural.dinobike.routes.ui.Routes
import es.softwarerural.dinobike.services.ui.Services
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

@Suppress("unused")
class DinobikeApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        configureDependencyInjection()
    }

    private fun configureDependencyInjection() {
        startKoin {
            //inject Android context
            androidContext(this@DinobikeApplication)
            // use Android logger - Level.INFO by default
            androidLogger()
            // use modules
        }
        Routes.init()
        Ideas.init()
        Services.init()
    }
}