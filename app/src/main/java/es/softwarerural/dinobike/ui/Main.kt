package es.softwarerural.dinobike.ui

import androidx.compose.material.BottomNavigation
import androidx.compose.material.BottomNavigationItem
import androidx.compose.material.Icon
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.navigation.NavHostController
import androidx.navigation.compose.KEY_ROUTE
import androidx.navigation.compose.currentBackStackEntryAsState
import androidx.navigation.compose.navigate
import androidx.navigation.compose.rememberNavController
import es.softwarerural.dinobike.ds.DinobikeTheme
import es.softwarerural.dinobike.routes.ui.ROUTES_NAVIGATION_ROUTE
import es.softwarerural.dinobike.services.ui.SERVICES_NAVIGATION_ROUTE

private val bottomNavigationItems = listOf(
    ROUTES_NAVIGATION_ROUTE,
    SERVICES_NAVIGATION_ROUTE,
)

@Composable
fun Main() {
    val navController = rememberNavController()
    DinobikeTheme {
        MainScreenNavigationConfigurations(
            navController
        ) { DinobikeBottomNavigation(navController) }

    }
}

@Composable
private fun DinobikeBottomNavigation(
    navController: NavHostController
) {
    BottomNavigation {
        val navBackStackEntry by navController.currentBackStackEntryAsState()
        val currentRoute =
            navBackStackEntry?.arguments?.getString(KEY_ROUTE) ?: ROUTES_NAVIGATION_ROUTE.route

        bottomNavigationItems.forEach { screen ->
            BottomNavigationItem(
                icon = { Icon(painter = painterResource(id = screen.icon), null) },
                label = { Text(stringResource(id = screen.resourceId)) },
                selected = currentRoute.startsWith(screen.route),
                alwaysShowLabel = false,
                onClick = {
                    navController.navigate(screen.route) {
                        // Pop up to the start destination of the graph to
                        // avoid building up a large stack of destinations
                        // on the back stack as users select items
                        popUpTo = navController.graph.startDestination
                        // Avoid multiple copies of the same destination when
                        // reselecting the same item
                        launchSingleTop = true
                    }
                }
            )
        }
    }
}