package es.softwarerural.dinobike.ui

import androidx.compose.runtime.Composable
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import es.softwarerural.dinobike.R
import es.softwarerural.dinobike.core.presentation.NavigationItem
import es.softwarerural.dinobike.home.ui.Home
import es.softwarerural.dinobike.ideas.ui.ideasNavigation
import es.softwarerural.dinobike.routes.ui.ROUTES_NAVIGATION_ROUTE
import es.softwarerural.dinobike.routes.ui.routesNavigation
import es.softwarerural.dinobike.services.ui.servicesNavigation

@Composable
internal fun MainScreenNavigationConfigurations(
    navController: NavHostController,
    bottomBar: @Composable () -> Unit = {}
) {
    NavHost(navController, startDestination = ROUTES_NAVIGATION_ROUTE.route) {
        routesNavigation(navController, bottomBar)
        servicesNavigation(navController, bottomBar)
    }
}