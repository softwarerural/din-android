import com.android.build.gradle.internal.cxx.configure.gradleLocalProperties
import es.softwarerural.dinobike.Dependencies
import es.softwarerural.dinobike.configureAndroid
import es.softwarerural.dinobike.configureCompose

plugins {
    id("com.android.application")
    kotlin("android")
    id("com.google.gms.google-services")
}

configureAndroid(
    versionName = "1.0.0",
    versionCode = 5
)
configureCompose()

android {
    defaultConfig {
        applicationId ="es.softwarerural.dinobike"
        manifestPlaceholders["googleMapsKey"] =
            gradleLocalProperties(rootDir).getProperty("google.maps.key", "asdf")
    }
    buildTypes {
        debug {
            applicationIdSuffix = ".debug"
        }
    }
}

dependencies {

    implementation(Dependencies.Kotlin.stdlib)
    implementation(Dependencies.AndroidX.coreKtx)
    implementation(Dependencies.Koin.android)

    implementation(Dependencies.AndroidX.Navigation.navigationCompose)

    implementation(platform("com.google.firebase:firebase-bom:27.1.0"))

    implementation("com.google.firebase:firebase-database-ktx")

    implementation(project(path = ":core:designsystem"))
    implementation(project(path = ":core:presentation"))
    implementation(project(path = ":home:presentation"))
    implementation(project(path = ":routes:presentation"))
    implementation(project(path = ":ideas:presentation"))
    implementation(project(path = ":services:presentation"))
}