package es.softwarerural.dinobike.ideas.domain.model

data class Idea(
    val id: String,
    val title: String,
    val description: String,
    val picture: String
)