package es.softwarerural.dinobike.ideas.domain

import es.softwarerural.dinobike.ideas.domain.usecases.GetIdeasUseCase
import org.koin.core.module.Module
import org.koin.dsl.module

val useCaseModule: Module = module {
    factory { GetIdeasUseCase(get()) }
}