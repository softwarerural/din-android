package es.softwarerural.dinobike.ideas.domain.usecases

import es.softwarerural.dinobike.ideas.domain.repository.IdeasRepository

class GetIdeasUseCase(
    private val ideasRepository: IdeasRepository
) {
    operator fun invoke() = ideasRepository.getIdeas()
}