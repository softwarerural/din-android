package es.softwarerural.dinobike.ideas.domain.repository

import es.softwarerural.dinobike.ideas.domain.model.Idea
import kotlinx.coroutines.flow.Flow

interface IdeasRepository {
    fun getIdeas(): Flow<Result<List<Idea>>>
    fun getIdea(ideaId: Int) : Idea
}