package es.softwarerural.dinobike.ideas.data

data class SuggestionDTO(
    val name: String = "",
    val description: String = "",
    val picture: String = ""
)