package es.softwarerural.dinobike.ideas.data

import es.softwarerural.dinobike.ideas.domain.repository.IdeasRepository
import org.koin.core.module.Module
import org.koin.dsl.module

val repositoryModule: Module = module {
    single {
        @Suppress("USELESS_CAST")
        FirebaseIdeasRepository() as IdeasRepository
    }
}