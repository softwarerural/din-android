package es.softwarerural.dinobike.ideas.data

import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.ktx.storage
import es.softwarerural.dinobike.firebase.fetchUrl
import es.softwarerural.dinobike.firebase.listFlow
import es.softwarerural.dinobike.ideas.domain.model.Idea
import es.softwarerural.dinobike.ideas.domain.repository.IdeasRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map

class FirebaseIdeasRepository(
    private val database: FirebaseDatabase =
        Firebase.database("https://dinobike-default-rtdb.europe-west1.firebasedatabase.app/"),
    private val storage: FirebaseStorage = Firebase.storage("gs://dinobike.appspot.com")
) : IdeasRepository {

    companion object {
        const val SUGGESTIONS_REFERENCE = "sugerencias"
    }

    override fun getIdeas(): Flow<Result<List<Idea>>> {
        return database.getReference(SUGGESTIONS_REFERENCE).listFlow<SuggestionDTO>()
            .map { dataSnapshot ->
                dataSnapshot.fold(
                    onFailure = { Result.failure(it) },
                    onSuccess = { list ->
                        val result = list.map { pair ->
                            val key = pair.first
                            val dto = pair.second
                            Idea(
                                key,
                                dto.name,
                                dto.description,
                                storage.fetchUrl(dto.picture)
                            )
                        }

                        Result.success(result)
                    }
                )
            }
    }

    override fun getIdea(ideaId: Int): Idea {
        TODO("Not yet implemented")
    }
}