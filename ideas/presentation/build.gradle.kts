plugins {
    id("es.softwarerural.dinobike.presentation")
}

dependencies {
    implementation(project(path = ":ideas:data"))
    implementation(project(path = ":ideas:domain", configuration = "default"))
}