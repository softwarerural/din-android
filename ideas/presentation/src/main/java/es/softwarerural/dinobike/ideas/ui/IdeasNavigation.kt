package es.softwarerural.dinobike.ideas.ui

import androidx.compose.runtime.Composable
import androidx.navigation.NavController
import androidx.navigation.NavGraphBuilder
import androidx.navigation.NavType
import androidx.navigation.compose.composable
import androidx.navigation.compose.navArgument
import androidx.navigation.compose.navigation
import es.softwarerural.dinobike.core.presentation.NavigationItem

internal const val IDEAS_ROUTE = "ideas"

val IDEAS_NAVIGATION_ROUTE = NavigationItem(
    IDEAS_ROUTE,
    R.string.ideas_route,
    R.drawable.ic_ideas
)

fun NavGraphBuilder.ideasNavigation(
    navController: NavController,
    bottomBar: @Composable () -> Unit = {}
) {
    navigation(
        startDestination = "$IDEAS_ROUTE.home",
        route = IDEAS_ROUTE
    ) {
        composable("$IDEAS_ROUTE.home") {
            Ideas(navController, bottomBar)
        }
        composable(
            route = "$IDEAS_ROUTE.details/{ideaId}",
            arguments = listOf(navArgument("ideaId") { type = NavType.IntType })

        ) { backStackEntry ->
            val ideaId = backStackEntry.arguments?.getInt("ideaId", 0) ?: 0
            IdeaDetails(ideaId, navController)
        }
    }
}