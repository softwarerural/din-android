package es.softwarerural.dinobike.ideas.ui

import androidx.lifecycle.ViewModel
import es.softwarerural.dinobike.ideas.domain.repository.IdeasRepository

class IdeaDetailsViewModel(
    ideaId: Int,
    repository: IdeasRepository
) : ViewModel() {

    val idea = repository.getIdea(ideaId)

}