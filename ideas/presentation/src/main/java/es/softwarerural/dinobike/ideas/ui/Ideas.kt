package es.softwarerural.dinobike.ideas.ui

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.navigation.NavController
import androidx.navigation.compose.navigate
import es.softwarerural.dinobike.core.presentation.getViewModel
import es.softwarerural.dinobike.ds.DinobikeTheme
import es.softwarerural.dinobike.ds.lce.LceEmpty
import es.softwarerural.dinobike.ds.lce.LceError
import es.softwarerural.dinobike.ds.lce.LceLoading
import es.softwarerural.dinobike.ds.preview.PreviewBottomBar
import es.softwarerural.dinobike.ideas.domain.model.Idea

@Composable
fun Ideas(
    navController: NavController,
    bottomBar: @Composable () -> Unit = {}
) {
    val viewModel: IdeasViewModel = getViewModel()
    StatelessIdeas(
        state = viewModel.state,
        bottomBar = bottomBar,
        { item -> navController.navigate("$IDEAS_ROUTE.details/${item.id}") },
        retry = viewModel::retry
    )
}

@Composable
fun StatelessIdeas(
    state: IdeasState,
    bottomBar: @Composable () -> Unit = {},
    goToDetails: (Idea) -> Unit = {},
    retry: () -> Unit = {}
) {
    Scaffold(
        topBar = {
            TopAppBar(
                title = {
                    Text(stringResource(id = R.string.ideas_route))
                }
            )
        },
        bottomBar = bottomBar
    ) { innerPadding ->
        Column(
            modifier = Modifier
                .padding(innerPadding)
        ) {
            when (state) {
                IdeasState.Loading -> LceLoading()
                IdeasState.Error -> LceError(retry = retry)
                IdeasState.Empty -> LceEmpty(
                    R.string.empty_ideas_list,
                    R.drawable.ic_ideas
                )
                is IdeasState.Data -> IdeasList(
                    items = state.data,
                    goToDetails = goToDetails
                )
            }
        }
    }
}

@Preview
@Composable
fun LoadingStateRoutesPreview() {
    DinobikeTheme {
        StatelessIdeas(
            IdeasState.Loading,
            bottomBar = { PreviewBottomBar() }
        )
    }
}

@Preview
@Composable
fun EmptyStateRoutesPreview() {
    DinobikeTheme {
        StatelessIdeas(
            IdeasState.Empty,
            bottomBar = { PreviewBottomBar() }
        )
    }
}

@Preview
@Composable
fun ErrorStateRoutesPreview() {
    DinobikeTheme {
        StatelessIdeas(
            IdeasState.Error,
            bottomBar = { PreviewBottomBar() }
        )
    }
}
//
//@Preview
//@Composable
//fun DataStateRoutesPreview() {
//    DinobikeTheme {
//        StatelessIdeas(
//            IdeasState.Data(fakeIdeas),
//            bottomBar = { PreviewBottomBar() }
//        )
//    }
//
//}