package es.softwarerural.dinobike.ideas.ui

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import es.softwarerural.dinobike.core.presentation.getViewModel
import es.softwarerural.dinobike.ds.DinobikeTheme
import es.softwarerural.dinobike.ds.molecules.images.DetailPicture
import es.softwarerural.dinobike.ds.navigation.BackTopAppBar
import es.softwarerural.dinobike.ideas.domain.model.Idea
import org.koin.core.parameter.parametersOf


@Composable
fun IdeaDetails(
    ideaId: Int,
    navController: NavController
) {
    val viewModel: IdeaDetailsViewModel = getViewModel { parametersOf(ideaId) }
    StatelessIdeaDetails(viewModel.idea) { navController.popBackStack() }
}

@Composable
private fun StatelessIdeaDetails(
    idea: Idea,
    upPress: () -> Unit
) {
    Column {
        BackTopAppBar(
            title = stringResource(id = R.string.idea_details_title),
            upPress = upPress
        )
        DetailPicture(
            pictureUrl = idea.picture,
            contentDescription = idea.title
        )
        Column(
            modifier = Modifier
                .padding(16.dp)
                .padding(top = 8.dp)
        ) {
            Text(idea.title, style = MaterialTheme.typography.h6)
            Spacer(modifier = Modifier.height(8.dp))
            Text(idea.description)
        }

    }
}

//@Preview
//@Composable
//private fun IdeaDetailsPreview() {
//    DinobikeTheme {
//        StatelessIdeaDetails(idea = fakeIdeas[0]) {}
//    }
//}