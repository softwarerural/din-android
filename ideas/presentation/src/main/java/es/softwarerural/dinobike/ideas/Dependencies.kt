package es.softwarerural.dinobike.ideas

import es.softwarerural.dinobike.ideas.data.repositoryModule
import es.softwarerural.dinobike.ideas.domain.useCaseModule
import es.softwarerural.dinobike.ideas.ui.IdeaDetailsViewModel
import es.softwarerural.dinobike.ideas.ui.IdeasViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.core.context.loadKoinModules
import org.koin.core.module.Module
import org.koin.dsl.module

private val viewModelModule: Module = module {
    viewModel { IdeasViewModel(get()) }
    viewModel { parameters -> IdeaDetailsViewModel(ideaId = parameters.get(), get()) }
}

object Ideas {
    fun init() = loadKoinModules(
        listOf(
            viewModelModule,
            useCaseModule,
            repositoryModule
        )
    )
}