package es.softwarerural.dinobike.ideas.ui

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import es.softwarerural.dinobike.ideas.domain.model.Idea
import es.softwarerural.dinobike.ideas.domain.usecases.GetIdeasUseCase
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach

class IdeasViewModel(
    private val getIdeas: GetIdeasUseCase
) : ViewModel() {

    var state: IdeasState by mutableStateOf(IdeasState.Loading)
        private set

    init {
        loadIdeas()
    }

    fun retry() {
        loadIdeas()
    }

    private fun loadIdeas() {
        getIdeas()
            .onEach { result ->
                state = result.fold(
                    onSuccess = this@IdeasViewModel::processSuccessfulState,
                    onFailure = { IdeasState.Error }
                )
            }
            .launchIn(viewModelScope)
    }

    private fun processSuccessfulState(result: List<Idea>) =
        if (result.isEmpty()) {
            IdeasState.Empty
        } else {
            IdeasState.Data(result)
        }
}