package es.softwarerural.dinobike.ideas.ui

import es.softwarerural.dinobike.ideas.domain.model.Idea

sealed class IdeasState {
    object Loading : IdeasState()
    object Empty : IdeasState()
    object Error : IdeasState()
    data class Data(val data: List<Idea>) : IdeasState()
}