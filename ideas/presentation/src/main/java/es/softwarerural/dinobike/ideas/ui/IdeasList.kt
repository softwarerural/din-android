package es.softwarerural.dinobike.ideas.ui

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.material.Card
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import es.softwarerural.dinobike.ds.molecules.images.DetailPicture
import es.softwarerural.dinobike.ds.text.CardHeader
import es.softwarerural.dinobike.ideas.domain.model.Idea

@Composable
fun IdeasList(
    items: List<Idea> = listOf(),
    goToDetails: (Idea) -> Unit = {}
) {
    val scrollState = rememberLazyListState()

    LazyColumn(
        contentPadding = PaddingValues(top = 8.dp),
        state = scrollState
    ) {
        items(items = items) { Idea ->
            IdeaItem(
                item = Idea,
                goToDetails = goToDetails
            )
        }
    }
}

@Composable
fun IdeaItem(
    item: Idea,
    goToDetails: (Idea) -> Unit = {}
) {
    Card(
        modifier = Modifier
            .fillMaxWidth()
            .padding(8.dp)
            .clickable { goToDetails(item) },
        elevation = 8.dp
    ) {
        Column {
            DetailPicture(
                pictureUrl = item.picture,
                contentDescription = item.title
            )
            CardHeader(item.title)
        }
    }
}

//@Preview
//@Composable
//fun IdeaItemPreview() {
//    DinobikeTheme {
//        IdeaItem(fakeIdeas[0].copy(picture = ""))
//    }
//}
//
//@Preview
//@Composable
//fun IdeasListPreview() {
//    DinobikeTheme {
//        IdeasList(fakeIdeas.map { it.copy(picture = "") })
//    }
//}