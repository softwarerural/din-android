package es.softwarerural.dinobike.routes.ui

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import es.softwarerural.dinobike.routes.domain.model.Route
import es.softwarerural.dinobike.routes.domain.usecases.GetRoutesUseCase
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach

class RoutesViewModel(
    private val getRoutes: GetRoutesUseCase
) : ViewModel() {

    var state: RoutesState by mutableStateOf(RoutesState.Loading)
        private set

    init {
        loadRoutes()
    }

    fun retry() {
        loadRoutes()
    }

    private fun loadRoutes() {
        getRoutes()
            .onEach { result ->
                state = result.fold(
                    onSuccess = this@RoutesViewModel::processSuccessfulState,
                    onFailure = { RoutesState.Error }
                )
            }
            .launchIn(viewModelScope)
    }

    private fun processSuccessfulState(result: List<Route>) =
        if (result.isEmpty()) {
            RoutesState.Empty
        } else {
            RoutesState.Data(result)
        }
}