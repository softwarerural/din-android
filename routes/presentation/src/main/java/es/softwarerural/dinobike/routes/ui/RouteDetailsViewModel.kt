package es.softwarerural.dinobike.routes.ui

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import es.softwarerural.dinobike.routes.domain.model.Route
import es.softwarerural.dinobike.routes.domain.model.RoutePath
import es.softwarerural.dinobike.routes.domain.repository.RoutesRepository
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch

class RouteDetailsViewModel(
    private val routeId: String,
    private val repository: RoutesRepository
) : ViewModel() {

    var state: RouteDetailsState by mutableStateOf(RouteDetailsState.Loading)
        private set

    init {
        loadRouteDetails()
    }

    private fun loadRouteDetails() {
        repository.getRoute(routeId)
            .onEach { result ->
                state = result.fold(
                    onSuccess = { route ->
                        RouteDetailsState.Data(
                            route,
                            RoutePathState.Loading
                        ).also {
                            loadRouteOnMap(it)
                        }
                    },
                    onFailure = { RouteDetailsState.Loading }
                )
            }
            .launchIn(viewModelScope)
    }

    private fun loadRouteOnMap(originalState: RouteDetailsState.Data) {
        viewModelScope.launch {
            val path = repository.getRoutePath(originalState.route.gpx)
            state = RouteDetailsState.Data(
                originalState.route,
                RoutePathState.Data(path))
        }
    }

}

sealed class RouteDetailsState(
    open val routePathState: RoutePathState
) {
    object Loading : RouteDetailsState(RoutePathState.Loading)

    data class Data(
        val route: Route,
        override val routePathState: RoutePathState
    ) : RouteDetailsState(routePathState)
}

sealed class RoutePathState {
    object Loading : RoutePathState()
    data class Data(val routePath: RoutePath) : RoutePathState()
}