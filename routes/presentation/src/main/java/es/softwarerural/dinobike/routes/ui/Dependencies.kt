package es.softwarerural.dinobike.routes.ui

import es.softwarerural.dinobike.routes.data.repositoryModule
import es.softwarerural.dinobike.routes.domain.useCaseModule
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.core.context.loadKoinModules
import org.koin.core.module.Module
import org.koin.dsl.module

private val viewModelModule: Module = module {
    viewModel { RoutesViewModel(get()) }
    viewModel { parameters -> RouteDetailsViewModel(routeId = parameters.get(), get()) }
}

object Routes {
    fun init() = loadKoinModules(
        listOf(
            viewModelModule,
            useCaseModule,
            repositoryModule
        )
    )
}