package es.softwarerural.dinobike.routes.ui

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.material.Icon
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.DirectionsBike
import androidx.compose.material.icons.filled.Error
import androidx.compose.material.icons.filled.Hiking
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.toArgb
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import com.google.android.libraries.maps.CameraUpdateFactory
import com.google.android.libraries.maps.GoogleMap
import com.google.android.libraries.maps.model.LatLng
import com.google.android.libraries.maps.model.LatLngBounds
import com.google.android.libraries.maps.model.PolylineOptions
import es.softwarerural.dinobike.core.presentation.getViewModel
import es.softwarerural.dinobike.ds.DinobikeTheme
import es.softwarerural.dinobike.ds.lce.LceLoading
import es.softwarerural.dinobike.ds.molecules.images.DetailPicture
import es.softwarerural.dinobike.ds.navigation.BackTopAppBar
import es.softwarerural.dinobike.mapview.MapViewContainer
import es.softwarerural.dinobike.mapview.rememberMapViewWithLifecycle
import es.softwarerural.dinobike.routes.data.fakeRoutes
import es.softwarerural.dinobike.routes.domain.model.Route
import es.softwarerural.dinobike.routes.domain.model.RouteDifficulty
import es.softwarerural.dinobike.routes.domain.model.RouteType
import org.koin.core.parameter.parametersOf

@Composable
fun RouteDetails(
    routeId: String,
    navController: NavController
) {
    val viewModel: RouteDetailsViewModel = getViewModel { parametersOf(routeId) }
    with(viewModel.state) {
        when (this) {
            is RouteDetailsState.Loading -> LceLoading()
            is RouteDetailsState.Data -> StatelessRouteDetails(
                route = route,
                mapView = { Map(viewModel) }
            ) { navController.popBackStack() }
        }
    }
}

@Composable
fun StatelessRouteDetails(
    route: Route,
    mapView: @Composable () -> Unit,
    upPress: () -> Unit = {}
) {
    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(MaterialTheme.colors.surface)
    ) {
        BackTopAppBar(
            title = stringResource(id = R.string.route_details_title),
            upPress = upPress
        )
        DetailPicture(
            pictureUrl = route.picture,
            contentDescription = route.name
        )

        Column(
            modifier = Modifier
                .padding(16.dp)
                .padding(top = 8.dp)
        ) {
            Row(
                modifier = Modifier.fillMaxWidth(),
                horizontalArrangement = Arrangement.SpaceBetween
            ) {
                Text(route.name, style = MaterialTheme.typography.h6)
                Icon(
                    imageVector = parseType(route.type),
                    contentDescription = null,
                    tint = MaterialTheme.colors.primary,
                    modifier = Modifier.padding(8.dp)
                )

            }
            Spacer(modifier = Modifier.height(8.dp))
            Row(
                modifier = Modifier.fillMaxWidth(),
                horizontalArrangement = Arrangement.SpaceEvenly
            ) {
                Column {
                    Text(text = "DISTANCIA: ${(route.distance ?: 0) / 1000}km")
                    Text(text = "DESNIVEL: ${route.elevation ?: 0}m")
                }
                Column {
                    Text(text = "DIFICULTAD: ${parseDifficulty(route)}")
                    Text(text = "DURACION: ${parseTime(route)}")
                }
            }
            Spacer(modifier = Modifier.height(8.dp))
            mapView()
        }
    }
}

private fun parseType(type: RouteType) = when (type) {
    RouteType.HIKING -> Icons.Filled.Hiking
    RouteType.MOUNTAIN_BIKE -> Icons.Filled.DirectionsBike
    RouteType.ROAD_BIKE -> Icons.Filled.DirectionsBike
    RouteType.UNKNOWN -> Icons.Filled.Error
}

private fun parseDifficulty(route: Route): String {
    return when (route.difficulty) {
        RouteDifficulty.VERY_LOW -> "Muy baja"
        RouteDifficulty.LOW -> "Baja"
        RouteDifficulty.MEDIUM -> "Media"
        RouteDifficulty.HIGH -> "Alta"
        RouteDifficulty.VERY_HIGH -> "Muy alta"
        RouteDifficulty.UNKNOWN -> "-"
    }
}

private fun parseTime(route: Route): String {
    return route.time?.let {
        if (it < 60) "${it}min"
        else if (it%60 > 0) "${(it/60)}h ${it%60}min"
        else "${(it/60)}h"
    } ?: "-"
}

@Composable
private fun Map(viewModel: RouteDetailsViewModel) {
    val mapView = rememberMapViewWithLifecycle()
    val pathState = viewModel.state.routePathState
    val routeColor = MaterialTheme.colors.secondary.toArgb()
    if (pathState is RoutePathState.Data) {
        MapViewContainer(map = mapView) { googleMap ->
            googleMap.mapType = GoogleMap.MAP_TYPE_SATELLITE
            val boundaries = LatLngBounds(
                LatLng(
                    pathState.routePath.boundaries.minLat,
                    pathState.routePath.boundaries.minLon
                ),  // SW bounds
                LatLng(
                    pathState.routePath.boundaries.maxLat,
                    pathState.routePath.boundaries.maxLon
                ) // NE bounds
            )
            googleMap.moveCamera(CameraUpdateFactory.newLatLngBounds(boundaries, 0))
            googleMap.addPolyline(
                PolylineOptions().apply {
                    color(routeColor)
                    for (point in pathState.routePath.points) {
                        add(LatLng(point.latitude, point.longitude))
                    }
                }.width(3f)

            )

        }
    }
}

@Preview
@Composable
private fun RouteDetailsPreview() {
    DinobikeTheme {
        StatelessRouteDetails(
            route = fakeRoutes[0],
            mapView = {
                Text(
                    "Aqui va el mapa", modifier = Modifier
                        .height(180.dp)
                        .fillMaxWidth()
                        .background(Color.Cyan)
                )
            }
        )
    }
}