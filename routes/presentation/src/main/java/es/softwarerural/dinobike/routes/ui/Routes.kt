package es.softwarerural.dinobike.routes.ui

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.navigation.NavController
import androidx.navigation.compose.navigate
import es.softwarerural.dinobike.core.presentation.getViewModel
import es.softwarerural.dinobike.ds.DinobikeTheme
import es.softwarerural.dinobike.ds.lce.LceEmpty
import es.softwarerural.dinobike.ds.lce.LceError
import es.softwarerural.dinobike.ds.lce.LceLoading
import es.softwarerural.dinobike.ds.preview.PreviewBottomBar
import es.softwarerural.dinobike.routes.data.fakeRoutes
import es.softwarerural.dinobike.routes.domain.model.Route

@Composable
fun Routes(
    navController: NavController,
    bottomBar: @Composable () -> Unit = {}
) {
    val viewModel: RoutesViewModel = getViewModel()
    StatelessRoutes(
        state = viewModel.state,
        bottomBar = bottomBar,
        { item -> navController.navigate("$ROUTES_ROUTE.details/${item.id}") },
        retry = viewModel::retry
    )
}

@Composable
fun StatelessRoutes(
    state: RoutesState,
    bottomBar: @Composable () -> Unit = {},
    goToDetails: (Route) -> Unit = {},
    retry: () -> Unit = {}
) {
    Scaffold(
        topBar = {
            TopAppBar(
                title = {
                    Text(stringResource(id = R.string.routes_route))
                }
            )
        },
        bottomBar = bottomBar
    ) { innerPadding ->
        Column(
            modifier = Modifier.padding(innerPadding)
        ) {
            when (state) {
                RoutesState.Loading -> LceLoading()
                RoutesState.Error -> LceError(retry = retry)
                RoutesState.Empty -> LceEmpty(
                    R.string.empty_routes_list,
                    R.drawable.ic_routes
                )
                is RoutesState.Data -> RoutesList(
                    state.data,
                    goToDetails
                )
            }
        }
    }
}

@Preview
@Composable
fun LoadingStateRoutesPreview() {
    DinobikeTheme {
        StatelessRoutes(
            RoutesState.Loading,
            bottomBar = { PreviewBottomBar() }
        )
    }
}

@Preview
@Composable
fun EmptyStateRoutesPreview() {
    DinobikeTheme {
        StatelessRoutes(
            RoutesState.Empty,
            bottomBar = { PreviewBottomBar() }
        )
    }
}

@Preview
@Composable
fun ErrorStateRoutesPreview() {
    DinobikeTheme {
        StatelessRoutes(
            RoutesState.Error,
            bottomBar = { PreviewBottomBar() }
        )
    }
}

@Preview
@Composable
fun DataStateRoutesPreview() {
    DinobikeTheme {
        StatelessRoutes(
            RoutesState.Data(fakeRoutes),
            bottomBar = { PreviewBottomBar() }
        )

    }
}