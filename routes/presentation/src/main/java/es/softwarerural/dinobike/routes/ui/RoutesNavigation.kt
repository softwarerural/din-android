package es.softwarerural.dinobike.routes.ui

import androidx.compose.runtime.Composable
import androidx.navigation.NavController
import androidx.navigation.NavGraphBuilder
import androidx.navigation.NavType
import androidx.navigation.compose.composable
import androidx.navigation.compose.navArgument
import androidx.navigation.compose.navigation
import es.softwarerural.dinobike.core.presentation.NavigationItem

internal const val ROUTES_ROUTE = "routes"

val ROUTES_NAVIGATION_ROUTE = NavigationItem(
    ROUTES_ROUTE,
    R.string.routes_route,
    R.drawable.ic_routes
)

fun NavGraphBuilder.routesNavigation(
    navController: NavController,
    bottomBar: @Composable () -> Unit = {}
) {
    navigation(
        startDestination = "$ROUTES_ROUTE.home",
        route = ROUTES_ROUTE
    ) {
        composable("$ROUTES_ROUTE.home") {
            Routes(navController, bottomBar)
        }
        composable(
            route = "$ROUTES_ROUTE.details/{routeId}",
            arguments = listOf(navArgument("routeId") { type = NavType.StringType })

        ) { backStackEntry ->
            val routeId = backStackEntry.arguments?.getString("routeId", "") ?: ""
            RouteDetails(routeId, navController)
        }
    }
}