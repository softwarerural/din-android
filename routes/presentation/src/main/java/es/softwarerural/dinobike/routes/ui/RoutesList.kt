package es.softwarerural.dinobike.routes.ui

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.material.Card
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import es.softwarerural.dinobike.ds.DinobikeTheme
import es.softwarerural.dinobike.ds.molecules.images.DetailPicture
import es.softwarerural.dinobike.ds.text.CardHeader
import es.softwarerural.dinobike.routes.data.fakeRoutes
import es.softwarerural.dinobike.routes.domain.model.Route

@Composable
fun RoutesList(
    items: List<Route> = listOf(),
    goToDetails: (Route) -> Unit = {}
) {
    val scrollState = rememberLazyListState()

    LazyColumn(
        contentPadding = PaddingValues(top = 8.dp),
        state = scrollState
    ) {
        items(items = items) { route ->
            RouteItem(
                item = route,
                goToDetails = goToDetails
            )
        }
    }
}

@Composable
fun RouteItem(
    item: Route,
    goToDetails: (Route) -> Unit = {}
) {
    Card(
        modifier = Modifier
            .fillMaxWidth()
            .padding(8.dp)
            .clickable { goToDetails(item) },
        elevation = 8.dp
    ) {
        Column {
            DetailPicture(
                pictureUrl = item.picture,
                contentDescription = item.name
            )
            CardHeader(item.name)
        }
    }
}

@Preview
@Composable
fun RouteItemPreview() {
    DinobikeTheme {
        RouteItem(fakeRoutes[0])
    }
}

@Preview
@Composable
fun RoutesListPreview() {
    DinobikeTheme {
        RoutesList(fakeRoutes)
    }
}