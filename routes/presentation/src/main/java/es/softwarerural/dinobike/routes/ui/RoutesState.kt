package es.softwarerural.dinobike.routes.ui

import es.softwarerural.dinobike.routes.domain.model.Route

sealed class RoutesState {
    object Loading : RoutesState()
    object Empty : RoutesState()
    object Error : RoutesState()
    data class Data(val data: List<Route>) : RoutesState()
}