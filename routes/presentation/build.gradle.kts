plugins {
    id("es.softwarerural.dinobike.presentation")
}

dependencies {
    implementation(project(path = ":generic:mapview"))
    implementation(project(path = ":routes:domain", configuration = "default"))
    implementation(project(path = ":routes:data"))
}