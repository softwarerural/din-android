package es.softwarerural.dinobike.routes.data

import com.google.common.truth.Truth.assertThat
import es.softwarerural.dinobike.routes.data.model.RouteDTO
import es.softwarerural.dinobike.routes.domain.model.Route
import es.softwarerural.dinobike.routes.domain.model.RouteDifficulty
import es.softwarerural.dinobike.routes.domain.model.RouteType
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Test
import java.lang.IllegalStateException

private const val ROUTE_ID = "the route id"
private const val ROUTE_NAME = "the route name"
private const val ROUTE_DESCRIPTION = "the route description"
private const val ROUTE_PICTURE = "the route picture"
private const val ROUTE_GPX = "the route gpx"
private const val ROUTE_DISTANCE = 1000L
private const val ROUTE_ELEVATION = 100L
private const val ROUTE_TIME = 60L

private const val ROUTE_PICTURE_URL = "the route picture url"
private const val ROUTE_GPX_URL = "the route gpx url"

class RouteMapperTest {
    private lateinit var storage: FirebaseStorageWrapper
    private lateinit var sut: RouteMapper

    @Before
    fun setUp() {
        storage = object:FirebaseStorageWrapper {
            override suspend fun fetchUrl(storageUrl: String): String {
                return when(storageUrl) {
                    ROUTE_GPX -> ROUTE_GPX_URL
                    ROUTE_PICTURE -> ROUTE_PICTURE_URL
                    else -> throw IllegalStateException("Bad url for storage : $storageUrl")
                }
            }

        }
        sut = RouteMapperImpl(storage)
    }

    @Test
    fun `should map defaults`() = runBlocking {
        val origin = buildDefaultOrigin()
        val expected = buildDefaultExpected()

        val result = sut(ROUTE_ID, origin)

        assertThat(result).isEqualTo(expected)
    }

    @Test
    fun `should map hiking routes`() = runBlocking {
        val origin = buildOrigin(type = 0)
        val expected = buildExpected(RouteType.HIKING)

        val result = sut(ROUTE_ID, origin)

        assertThat(result).isEqualTo(expected)
    }

    private fun buildDefaultOrigin() = RouteDTO(
        name = ROUTE_NAME,
        description = ROUTE_DESCRIPTION,
        picture = ROUTE_PICTURE,
        gpx = ROUTE_GPX,
    )

    private fun buildDefaultExpected() = Route(
        id = ROUTE_ID,
        name = ROUTE_NAME,
        description = ROUTE_DESCRIPTION,
        picture = ROUTE_PICTURE_URL,
        gpx = ROUTE_GPX_URL
    )

    @Test
    fun `should map mountain bike routes`() = runBlocking {
        val origin = buildOrigin(type = 1)
        val expected = buildExpected(RouteType.MOUNTAIN_BIKE)

        val result = sut(ROUTE_ID, origin)

        assertThat(result).isEqualTo(expected)
    }

    @Test
    fun `should map road bike routes`() = runBlocking {
        val origin = buildOrigin(type = 2)
        val expected = buildExpected(RouteType.ROAD_BIKE)

        val result = sut(ROUTE_ID, origin)

        assertThat(result).isEqualTo(expected)
    }

    @Test
    fun `should map routes with unknown type`() = runBlocking {
        val origin = buildOrigin(type = Long.MAX_VALUE)
        val expected = buildExpected(RouteType.UNKNOWN)

        val result = sut(ROUTE_ID, origin)

        assertThat(result).isEqualTo(expected)
    }

    @Test
    fun `should map very low difficulty routes`() = runBlocking {
        val origin = buildOrigin(difficulty = 0)
        val expected = buildExpected(difficulty = RouteDifficulty.VERY_LOW)

        val result = sut(ROUTE_ID, origin)

        assertThat(result).isEqualTo(expected)
    }

    @Test
    fun `should map low difficulty routes`() = runBlocking {
        val origin = buildOrigin(difficulty = 1)
        val expected = buildExpected(difficulty = RouteDifficulty.LOW)

        val result = sut(ROUTE_ID, origin)

        assertThat(result).isEqualTo(expected)
    }

    @Test
    fun `should map medium difficulty routes`() = runBlocking {
        val origin = buildOrigin(difficulty = 2)
        val expected = buildExpected(difficulty = RouteDifficulty.MEDIUM)

        val result = sut(ROUTE_ID, origin)

        assertThat(result).isEqualTo(expected)
    }
    @Test
    fun `should map high difficulty routes`() = runBlocking {
        val origin = buildOrigin(difficulty = 3)
        val expected = buildExpected(difficulty = RouteDifficulty.HIGH)

        val result = sut(ROUTE_ID, origin)

        assertThat(result).isEqualTo(expected)
    }

    @Test
    fun `should map very high difficulty routes`() = runBlocking {
        val origin = buildOrigin(difficulty = 4)
        val expected = buildExpected(difficulty = RouteDifficulty.VERY_HIGH)

        val result = sut(ROUTE_ID, origin)

        assertThat(result).isEqualTo(expected)
    }

    @Test
    fun `should map routes with unknown difficulty`() = runBlocking {
        val origin = buildOrigin(difficulty = Long.MAX_VALUE)
        val expected = buildExpected(difficulty = RouteDifficulty.UNKNOWN)

        val result = sut(ROUTE_ID, origin)

        assertThat(result).isEqualTo(expected)
    }

    private fun buildOrigin(
        type: Long? = null,
        difficulty: Long? = null
    ) = RouteDTO(
        name = ROUTE_NAME,
        routeType = type,
        description = ROUTE_DESCRIPTION,
        picture = ROUTE_PICTURE,
        gpx = ROUTE_GPX,
        distance = ROUTE_DISTANCE,
        elevation = ROUTE_ELEVATION,
        time = ROUTE_TIME,
        difficulty = difficulty
    )

    private fun buildExpected(
        type: RouteType = RouteType.UNKNOWN,
        difficulty: RouteDifficulty = RouteDifficulty.UNKNOWN
    ) = Route(
        id = ROUTE_ID,
        name = ROUTE_NAME,
        type = type,
        description = ROUTE_DESCRIPTION,
        picture = ROUTE_PICTURE_URL,
        gpx = ROUTE_GPX_URL,
        distance = ROUTE_DISTANCE.toInt(),
        elevation = ROUTE_ELEVATION.toInt(),
        time = ROUTE_TIME.toInt(),
        difficulty = difficulty
    )
}