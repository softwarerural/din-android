package es.softwarerural.dinobike.routes.data.model

data class RouteDTO(
    val name: String = "",
    val description: String = "",
    val picture: String = "",
    val gpx: String = "",
    val difficulty: Long? = null,
    val distance: Long? = null,
    val elevation: Long? = null,
    val routeType: Long? = null,
    val time: Long? = null
)