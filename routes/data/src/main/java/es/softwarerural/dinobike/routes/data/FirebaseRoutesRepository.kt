package es.softwarerural.dinobike.routes.data

import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase
import es.softwarerural.dinobike.firebase.listFlow
import es.softwarerural.dinobike.firebase.valueFlow
import es.softwarerural.dinobike.gpx.GpxReader
import es.softwarerural.dinobike.routes.data.model.RouteDTO
import es.softwarerural.dinobike.routes.domain.model.Point
import es.softwarerural.dinobike.routes.domain.model.Route
import es.softwarerural.dinobike.routes.domain.model.RouteBoundaries
import es.softwarerural.dinobike.routes.domain.model.RoutePath
import es.softwarerural.dinobike.routes.domain.repository.RoutesRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.withContext
import kotlin.math.max
import kotlin.math.min

internal class FirebaseRoutesRepository(
    private val mapRoute: RouteMapper,
    private val database: FirebaseDatabase =
        Firebase.database("https://dinobike-default-rtdb.europe-west1.firebasedatabase.app/")
) : RoutesRepository {

    companion object {
        const val ROUTES_REFERENCE = "rutas"
    }

    override fun getRoutes(): Flow<Result<List<Route>>> {
        return database.getReference(ROUTES_REFERENCE).listFlow<RouteDTO>()
            .map { dataSnapshot ->
                dataSnapshot.fold(
                    onFailure = { Result.failure(it) },
                    onSuccess = { list ->
                        val result = list.map { pair ->
                            mapRoute(pair.first, pair.second)
                        }

                        Result.success(result)
                    }
                )
            }
    }

    @ExperimentalCoroutinesApi
    override fun getRoute(routeId: String): Flow<Result<Route>> {
        return database.getReference("$ROUTES_REFERENCE/$routeId").valueFlow<RouteDTO>()
            .map { result ->
                result.fold(
                    onFailure = { Result.failure(it) },
                    onSuccess = { pair ->
                        val route = mapRoute(pair.first, pair.second)
                        Result.success(route)
                    }
                )
            }
    }

    override suspend fun getRoutePath(gpxUrl: String): RoutePath = withContext(Dispatchers.IO) {
        var maxLatitude = Double.NEGATIVE_INFINITY
        var minLatitude = Double.POSITIVE_INFINITY
        var maxLongitude = Double.NEGATIVE_INFINITY
        var minLongitude = Double.POSITIVE_INFINITY
        val points = GpxReader().read(gpxUrl).map {
            maxLatitude = max(maxLatitude, it.latitude)
            minLatitude = min(minLatitude, it.latitude)
            maxLongitude = max(maxLongitude, it.longitude)
            minLongitude = min(minLongitude, it.longitude)
            Point(
                it.latitude,
                it.longitude,
                it.altitude
            )
        }
        val latitudeIncrement = maxLatitude - minLatitude
        maxLatitude += latitudeIncrement / 10
        minLatitude -= latitudeIncrement / 10
        val longitudeIncrement = maxLongitude - minLongitude
        maxLongitude += longitudeIncrement / 10
        minLongitude -= longitudeIncrement / 10

        RoutePath(
            points,
            RouteBoundaries(
                minLatitude,
                minLongitude,
                maxLatitude,
                maxLongitude
            )
        )
    }
}