package es.softwarerural.dinobike.routes.data

import es.softwarerural.dinobike.routes.data.model.RouteDTO
import es.softwarerural.dinobike.routes.domain.model.Route
import es.softwarerural.dinobike.routes.domain.model.RouteDifficulty
import es.softwarerural.dinobike.routes.domain.model.RouteType

internal interface RouteMapper {
    suspend operator fun invoke(key: String, origin: RouteDTO): Route
}

internal class RouteMapperImpl(
    private val storage: FirebaseStorageWrapper
) : RouteMapper {
    override suspend fun invoke(key: String, origin: RouteDTO): Route {
            return Route(
                id = key,
                name = origin.name,
                type = mapType(origin.routeType),
                description = origin.description,
                picture = storage.fetchUrl(origin.picture),
                gpx = storage.fetchUrl(origin.gpx),
                elevation = origin.elevation?.toInt(),
                distance = origin.distance?.toInt(),
                time = origin.time?.toInt(),
                difficulty = mapDifficulty(origin.difficulty)
            )
    }

    private fun mapType(type: Long?) : RouteType {
        return when(type) {
            0L -> RouteType.HIKING
            1L -> RouteType.MOUNTAIN_BIKE
            2L -> RouteType.ROAD_BIKE
            else -> RouteType.UNKNOWN
        }
    }

    private fun mapDifficulty(difficulty: Long?) = when(difficulty) {
        0L -> RouteDifficulty.VERY_LOW
        1L -> RouteDifficulty.LOW
        2L -> RouteDifficulty.MEDIUM
        3L -> RouteDifficulty.HIGH
        4L -> RouteDifficulty.VERY_HIGH
        else -> RouteDifficulty.UNKNOWN
    }
}