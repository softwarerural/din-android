package es.softwarerural.dinobike.routes.data

import es.softwarerural.dinobike.routes.domain.model.Route
import es.softwarerural.dinobike.routes.domain.repository.RoutesRepository
import org.koin.core.module.Module
import org.koin.dsl.module

@Suppress("USELESS_CAST")
val repositoryModule: Module = module {
    single {
        FirebaseRoutesRepository(get()) as RoutesRepository
    }
    single {
        FirebaseStorageWrapperImpl() as FirebaseStorageWrapper
    }
    factory { RouteMapperImpl(get()) as RouteMapper }
}