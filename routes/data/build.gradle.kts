plugins {
    id("es.softwarerural.dinobike.data")
}

dependencies {
    implementation(project(path = ":routes:domain", configuration = "default"))
    implementation(project(path = ":generic:firebase"))
    implementation(project(path = ":generic:gpx"))

    implementation(platform("com.google.firebase:firebase-bom:27.1.0"))

    implementation("com.google.firebase:firebase-database-ktx")
    implementation("com.google.firebase:firebase-storage-ktx")
}