-dontobfuscate

-keepattributes Signature

-keepclassmembers class es.softwarerural.dinobike.routes.data.model.** {
  *;
}

-keep,allowoptimization class com.google.android.libraries.maps.** { *; }