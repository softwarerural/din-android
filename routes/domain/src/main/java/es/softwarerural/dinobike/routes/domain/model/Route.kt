package es.softwarerural.dinobike.routes.domain.model

data class Route(
    val id: String,
    val name: String,
    val type: RouteType = RouteType.UNKNOWN,
    val description: String,
    val picture: String,
    val gpx: String,
    val distance: Int? = null,
    val elevation: Int? = null,
    val difficulty: RouteDifficulty = RouteDifficulty.UNKNOWN,
    val time: Int? = null
)