package es.softwarerural.dinobike.routes.domain.model

enum class RouteType {
    HIKING,
    MOUNTAIN_BIKE,
    ROAD_BIKE,
    UNKNOWN
}