package es.softwarerural.dinobike.routes.domain.usecases

import es.softwarerural.dinobike.routes.domain.repository.RoutesRepository

class GetRoutesUseCase(
    private val routesRepository: RoutesRepository
) {
    operator fun invoke() = routesRepository.getRoutes()
}