package es.softwarerural.dinobike.routes.domain.repository

import es.softwarerural.dinobike.routes.domain.model.Route
import es.softwarerural.dinobike.routes.domain.model.RoutePath
import kotlinx.coroutines.flow.Flow

interface RoutesRepository {
    fun getRoutes() : Flow<Result<List<Route>>>
    fun getRoute(routeId: String) : Flow<Result<Route>>
    suspend fun getRoutePath(gpxUrl: String) : RoutePath
}