package es.softwarerural.dinobike.routes.domain.model

data class RoutePath(
    val points: List<Point>,
    val boundaries: RouteBoundaries
)