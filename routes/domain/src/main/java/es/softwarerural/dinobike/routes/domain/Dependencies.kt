package es.softwarerural.dinobike.routes.domain

import es.softwarerural.dinobike.routes.domain.usecases.GetRoutesUseCase
import org.koin.core.module.Module
import org.koin.dsl.module

val useCaseModule : Module = module {
    factory { GetRoutesUseCase(get()) }
}