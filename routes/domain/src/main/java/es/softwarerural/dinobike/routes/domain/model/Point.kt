package es.softwarerural.dinobike.routes.domain.model

data class Point (
    val latitude: Double,
    val longitude: Double,
    val altitude: Double
)