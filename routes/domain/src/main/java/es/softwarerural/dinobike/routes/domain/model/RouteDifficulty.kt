package es.softwarerural.dinobike.routes.domain.model

enum class RouteDifficulty {
    VERY_LOW,
    LOW,
    MEDIUM,
    HIGH,
    VERY_HIGH,
    UNKNOWN
}