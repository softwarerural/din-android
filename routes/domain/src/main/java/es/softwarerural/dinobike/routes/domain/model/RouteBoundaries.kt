package es.softwarerural.dinobike.routes.domain.model

data class RouteBoundaries(
    val minLat: Double,
    val minLon: Double,
    val maxLat: Double,
    val maxLon: Double
)