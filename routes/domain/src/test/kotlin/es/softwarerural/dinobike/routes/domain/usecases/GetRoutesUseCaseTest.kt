package es.softwarerural.dinobike.routes.domain.usecases

import com.google.common.truth.Truth.assertThat
import es.softwarerural.dinobike.routes.domain.model.Route
import es.softwarerural.dinobike.routes.domain.model.RoutePath
import es.softwarerural.dinobike.routes.domain.repository.RoutesRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.runBlocking
import org.junit.Test

internal class GetRoutesUseCaseTest {

    @Test
    fun `invoke SHOULD return repository result`() = runBlocking {
        val repository = RoutesRepositoryStub()
        val sut = GetRoutesUseCase(repository)

        val result = sut().first()

        assertThat(result).isEqualTo(repository.routes)
    }

    class RoutesRepositoryStub: RoutesRepository {
        val routes : Result<List<Route>> = Result.success(emptyList())
        override fun getRoutes() = flow {
            emit(routes)
        }

        override fun getRoute(routeId: String): Flow<Result<Route>> {
            TODO("Not yet implemented")
        }

        override suspend fun getRoutePath(routeId: String): RoutePath {
            TODO("Not yet implemented")
        }

    }
}