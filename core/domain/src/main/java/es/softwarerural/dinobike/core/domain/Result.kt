package es.softwarerural.dinobike.core.domain

sealed class Result<out T> {
    data class Success<out T>(val data: T) : Result<T>()
    data class Failure(val error: Exception) : Result<Nothing>()

    fun <R> map(onSuccess: (T) -> R, onFailure: (Exception) -> R) : R {
        return when(this) {
            is Success -> onSuccess(data)
            is Failure -> onFailure(error)
        }
    }
}