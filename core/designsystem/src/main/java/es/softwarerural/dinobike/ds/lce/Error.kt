package es.softwarerural.dinobike.ds.lce

import androidx.annotation.StringRes
import androidx.compose.foundation.layout.*
import androidx.compose.material.FloatingActionButton
import androidx.compose.material.Icon
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.Autorenew
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import es.softwarerural.dinobike.ds.DinobikeTheme
import es.softwarerural.dinobike.ds.R

@Composable
fun LceError(
    @StringRes message: Int = R.string.default_lce_error,
    retry: () -> Unit
) {
    Column(
        modifier = Modifier
            .fillMaxWidth()
            .fillMaxHeight(),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        FloatingActionButton(
            onClick = retry
        ) {
            Icon(Icons.Rounded.Autorenew, null)
        }
        Spacer(modifier = Modifier.height(8.dp))
        Text(stringResource(id = message))
    }
}

@Preview
@Composable
fun ErrorPreview() {
    DinobikeTheme {
        LceError(retry = {})
    }
}