package es.softwarerural.dinobike.ds.lce

import androidx.annotation.StringRes
import androidx.compose.foundation.layout.*
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import es.softwarerural.dinobike.ds.DinobikeTheme
import es.softwarerural.dinobike.ds.R

@Composable
fun LceLoading(
    @StringRes message: Int? = null
) {
    Column(
        modifier = Modifier
            .fillMaxWidth()
            .fillMaxHeight(),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        CircularProgressIndicator()
        message?.let {
            Spacer(modifier = Modifier.height(8.dp))
            Text(stringResource(id = it))
        }
    }
}

@Preview
@Composable
fun LoadingWithoutMessagePreview() {
    DinobikeTheme {
        LceLoading()
    }
}

@Preview
@Composable
fun LoadingWithMessagePreview() {
    DinobikeTheme {
        LceLoading(message = R.string.default_loading_message)
    }
}