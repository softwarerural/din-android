package es.softwarerural.dinobike.ds.lce

import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import androidx.compose.foundation.layout.*
import androidx.compose.material.Icon
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import es.softwarerural.dinobike.ds.DinobikeTheme
import es.softwarerural.dinobike.ds.R

@Composable
fun LceEmpty(
    @StringRes message: Int = R.string.default_empty_collection,
    @DrawableRes icon: Int = R.drawable.ic_baseline_report_problem_24
) {
    Column(
        modifier = Modifier
            .fillMaxWidth()
            .fillMaxHeight(),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Icon(
            painter = painterResource(id = icon),
            contentDescription = null,
            modifier = Modifier
                .width(64.dp)
                .height(64.dp),
            tint = MaterialTheme.colors.primary
        )
        Spacer(modifier = Modifier.height(8.dp))
        Text(stringResource(id = message))
    }
}

@Preview
@Composable
fun EmptyPreview() {
    DinobikeTheme {
        LceEmpty()
    }
}