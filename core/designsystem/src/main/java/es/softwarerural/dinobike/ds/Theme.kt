package es.softwarerural.dinobike.ds

import androidx.compose.material.MaterialTheme
import androidx.compose.material.lightColors
import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.Color

private val asdf = Color(0xffffc66f)
private val marron = Color(0xffc17946)
private val LightColors = lightColors(
    primary = marron,
    background = asdf,
    surface = Color.White,
    onPrimary = Color.Black
)

@Composable
fun DinobikeTheme(
    content: @Composable () -> Unit
) {
    MaterialTheme(
        content = content,
        colors = LightColors
    )
}