package es.softwarerural.dinobike.ds.molecules.images

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.Icon
import androidx.compose.material.MaterialTheme
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.BrokenImage
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.google.accompanist.coil.rememberCoilPainter
import com.google.accompanist.imageloading.ImageLoadState
import es.softwarerural.dinobike.ds.DinobikeTheme
import es.softwarerural.dinobike.ds.R

@Composable
fun DetailPicture(
    pictureUrl: String,
    contentDescription: String?,
    modifier: Modifier = Modifier
) {
    val pictureModifier = modifier
        .height(180.dp)
        .fillMaxWidth()
    if (pictureUrl == "") {
        Placeholder(contentDescription, pictureModifier)
    } else {
        val painter = rememberCoilPainter(request = pictureUrl)
        Box {
            Image(
                painter = painter,
                contentDescription = contentDescription,
                modifier = pictureModifier,
                contentScale = ContentScale.Crop
            )
            when (painter.loadState) {
                is ImageLoadState.Loading -> {
                    Loading()
                }
                is ImageLoadState.Error -> {
                    Error(contentDescription = contentDescription)
                }
            }
        }
    }
}

@Composable
private fun Placeholder(
    contentDescription: String?,
    modifier: Modifier = Modifier
) {
    Image(
        painter = painterResource(R.drawable.picture_placeholder),
        contentDescription = contentDescription,
        modifier = modifier,
        contentScale = ContentScale.Crop
    )
}

@Composable
private fun BoxScope.Loading() {
    Box(Modifier.matchParentSize()) {
        Placeholder(contentDescription = null, modifier = Modifier.matchParentSize())
        Box(
            modifier = Modifier
                .background(Color.White.copy(alpha = 0.75f))
                .matchParentSize()
        )
        CircularProgressIndicator(Modifier.align(Alignment.Center))
    }
}

@Composable
private fun BoxScope.Error(
    contentDescription: String?
) {
    Box(Modifier.matchParentSize()) {
        Placeholder(contentDescription = contentDescription, modifier = Modifier.matchParentSize())
        Box(
            modifier = Modifier
                .background(MaterialTheme.colors.surface.copy(alpha = 0.5f))
                .matchParentSize()
        )
        Icon(
            imageVector = Icons.Filled.BrokenImage,
            contentDescription = null,
            tint = MaterialTheme.colors.primary,
            modifier = Modifier.padding(8.dp)
        )
    }
}

@Preview
@Composable
private fun PlaceholderPreview() {
    DinobikeTheme {
        DetailPicture(
            pictureUrl = "",
            contentDescription = "Placeholder"
        )
    }
}

@Preview
@Composable
private fun ModifiedDimensionsPlaceholderPreview() {
    DinobikeTheme {
        DetailPicture(
            pictureUrl = "",
            contentDescription = "Placeholder",
            modifier = Modifier
                .width(90.dp)
                .height(90.dp)
        )
    }
}

//TODO once CoilImage works with preview, use DetailPicture directly
@Preview
@Composable
private fun LoadingPreview() {
    DinobikeTheme {
        Box(
            modifier = Modifier
                .height(180.dp)
                .fillMaxWidth()
        ) {
            Loading()
        }
    }
}

@Preview
@Composable
private fun ErrorPreview() {
    DinobikeTheme {
        Box(
            modifier = Modifier
                .height(180.dp)
                .fillMaxWidth()
        ) {
            Error(null)
        }
    }
}