package es.softwarerural.dinobike.ds.text

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Card
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import es.softwarerural.dinobike.ds.DinobikeTheme
import es.softwarerural.dinobike.ds.molecules.images.DetailPicture

@Composable
fun CardHeader(content: String) {
    Text(
        text = content,
        style = MaterialTheme.typography.h6,
        modifier = Modifier.padding(8.dp)
    )
}

@Preview
@Composable
fun CardHeaderPreview() {
    DinobikeTheme {
        Card {
            Column {
                DetailPicture(
                    pictureUrl = "",
                    contentDescription = null
                )
                CardHeader("Ejemplo de encabezado")
            }
        }
    }
}