import es.softwarerural.dinobike.Dependencies
import es.softwarerural.dinobike.configureCompose

plugins {
    id("es.softwarerural.dinobike.android.library")
}

configureCompose()

dependencies {

    implementation(Dependencies.AndroidX.coreKtx)

    api(Dependencies.AndroidX.Compose.ui)
    api(Dependencies.AndroidX.Compose.material)
    api(Dependencies.AndroidX.Compose.iconsExtended)
    api(Dependencies.AndroidX.Activity.activityCompose)
    api(Dependencies.AndroidX.Compose.tooling)

    api(Dependencies.Accompanist)
}