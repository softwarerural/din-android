import es.softwarerural.dinobike.Dependencies
import es.softwarerural.dinobike.Dependencies.Koin.implementKoin
import es.softwarerural.dinobike.configureCompose

plugins {
    id("es.softwarerural.dinobike.android.library")
}

configureCompose()

dependencies {
    implementation(Dependencies.AndroidX.Compose.runtime)
    implementation(Dependencies.AndroidX.Lifecycle.viewModelCompose)
    implementKoin()
}