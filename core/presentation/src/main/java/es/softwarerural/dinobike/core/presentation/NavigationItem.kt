package es.softwarerural.dinobike.core.presentation

import androidx.annotation.DrawableRes
import androidx.annotation.StringRes

data class NavigationItem(
    val route: String,
    @StringRes val resourceId: Int,
    @DrawableRes val icon: Int
)