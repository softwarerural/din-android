rootProject.name = "Dinobike"

include(":app")
include(
    ":core:domain",
    ":core:designsystem",
    ":core:presentation"
)
include(":generic:mapview")
include(":home:presentation")
include(
    ":routes:data",
    ":routes:domain",
    ":routes:presentation"
)
include(
    ":ideas:data",
    ":ideas:domain",
    ":ideas:presentation"
)
include(
    ":services:data",
    ":services:domain",
    ":services:presentation"
)
include(":generic:gpx")
include(":generic:firebase")
