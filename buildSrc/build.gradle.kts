plugins {
    `kotlin-dsl`
}
repositories {
    jcenter()
    google()
}

dependencies {
    // android gradle plugin, required by custom plugin
    implementation("com.android.tools.build:gradle:7.0.0-beta04")

    // kotlin plugin, required by custom plugin
    implementation(kotlin("gradle-plugin", "1.4.32"))

    implementation(gradleApi())
    implementation(localGroovy())
}