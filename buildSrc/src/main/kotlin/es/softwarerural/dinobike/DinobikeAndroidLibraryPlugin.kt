package es.softwarerural.dinobike

import com.android.build.gradle.internal.utils.KOTLIN_ANDROID_PLUGIN_ID
import org.gradle.api.Plugin
import org.gradle.api.Project

open class DinobikeAndroidLibraryPlugin : Plugin<Project> {
    override fun apply(target: Project) {
        with(target) {
            configurePlugins()
            configureAndroid()
        }
    }

    private fun Project.configurePlugins() {
        plugins.apply("com.android.library")
        plugins.apply(KOTLIN_ANDROID_PLUGIN_ID)
    }
}

