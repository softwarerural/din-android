package es.softwarerural.dinobike

import org.gradle.api.artifacts.dsl.DependencyHandler

object Dependencies {
    object Kotlin {
        const val version = "1.4.32"
        const val stdlib = "org.jetbrains.kotlin:kotlin-stdlib:$version"
        const val reflect = "org.jetbrains.kotlin:kotlin-reflect:$version"
    }

    object AndroidX {
        const val coreKtx = "androidx.core:core-ktx:1.3.2"

        object Compose {
            const val version = "1.0.0-beta05"

            const val foundation = "androidx.compose.foundation:foundation:$version"
            const val layout = "androidx.compose.foundation:foundation-layout:$version"
            const val ui = "androidx.compose.ui:ui:$version"
            const val runtime = "androidx.compose.runtime:runtime:$version"
            const val liveData = "androidx.compose.runtime:runtime-livedata:$version"
            const val material = "androidx.compose.material:material:$version"
            const val tooling = "androidx.compose.ui:ui-tooling:$version"
            const val iconsExtended = "androidx.compose.material:material-icons-extended:$version"
        }

        object Activity {
            const val activityCompose = "androidx.activity:activity-compose:1.3.0-alpha03"
        }

        object Navigation {
            const val navigationCompose = "androidx.navigation:navigation-compose:1.0.0-alpha08"
        }

        object Lifecycle {
            const val viewModelCompose = "androidx.lifecycle:lifecycle-viewmodel-compose:1.0.0-alpha02"
            const val viewModelKtx = "androidx.lifecycle:lifecycle-viewmodel-ktx:2.3.0"
        }
    }

    object KotlinX {
        const val version = "1.4.3"
        const val core = "org.jetbrains.kotlinx:kotlinx-coroutines-core:$version"
        const val test = "org.jetbrains.kotlinx:kotlinx-coroutines-test:$version"
    }

    object Koin {
        private const val version = "2.2.2"
        const val core = "org.koin:koin-core:$version"
        const val android = "org.koin:koin-android:$version"
        const val viewModel = "org.koin:koin-android-viewmodel:$version"
        const val compose = "org.koin:koin-androidx-compose:$version"

        fun DependencyHandler.implementKoin() {
            add("implementation", android)
            add("implementation", viewModel)
            add("implementation", compose)
        }
    }

    const val Accompanist = "com.google.accompanist:accompanist-coil:0.8.0"

    const val JUnit = "junit:junit:4.13.2"

    object Truth {
        private const val version = "1.1.2"
        const val truth = "com.google.truth:truth:$version"
        const val java8 = "com.google.truth.extensions:truth-java8-extension:$version"
    }
}