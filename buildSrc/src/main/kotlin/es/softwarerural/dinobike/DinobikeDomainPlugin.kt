package es.softwarerural.dinobike

import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.kotlin.dsl.dependencies
import org.gradle.kotlin.dsl.project

open class DinobikeDomainPlugin : Plugin<Project> {
    override fun apply(target: Project) {
        with(target) {
            plugins.apply("es.softwarerural.dinobike.java.library")
            configureDependencies()
        }
    }

    private fun Project.configureDependencies() = dependencies {
        add("implementation", project(path = ":core:domain", configuration = "default"))
        add("implementation", Dependencies.Koin.core)
        add("implementation", Dependencies.KotlinX.core)

        add("testImplementation", Dependencies.JUnit)
        add("testImplementation", Dependencies.KotlinX.test)
        add("testImplementation", Dependencies.Truth.truth)
        add("testImplementation", Dependencies.Truth.java8)
    }
}

