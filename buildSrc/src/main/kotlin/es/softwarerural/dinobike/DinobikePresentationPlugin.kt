package es.softwarerural.dinobike

import es.softwarerural.dinobike.Dependencies.Koin.implementKoin
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.kotlin.dsl.dependencies
import org.gradle.kotlin.dsl.project

open class DinobikePresentationPlugin : Plugin<Project> {
    override fun apply(target: Project) {
        with(target) {
            plugins.apply("es.softwarerural.dinobike.android.library")
            configureCompose()
            configureDependencies()
        }
    }

    private fun Project.configureDependencies() = dependencies {
        add("implementation", project(path = ":core:designsystem"))
        add("implementation", project(path = ":core:presentation"))
        add("implementation", Dependencies.AndroidX.Compose.liveData)
        add("implementation", Dependencies.AndroidX.Compose.foundation)
        add("implementation", Dependencies.AndroidX.Compose.layout)
        add("implementation", Dependencies.AndroidX.Navigation.navigationCompose)
        add("implementation", Dependencies.AndroidX.Lifecycle.viewModelCompose)
        add("implementation", Dependencies.AndroidX.Lifecycle.viewModelKtx)
        implementKoin()
    }
}

