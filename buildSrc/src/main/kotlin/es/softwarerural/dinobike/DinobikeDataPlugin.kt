package es.softwarerural.dinobike

import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.kotlin.dsl.dependencies

open class DinobikeDataPlugin : Plugin<Project> {
    override fun apply(target: Project) {
        with(target) {
            plugins.apply("es.softwarerural.dinobike.android.library")
            configureDependencies()
        }
    }

    private fun Project.configureDependencies() = dependencies {
        add("implementation", Dependencies.Kotlin.stdlib)
        add("implementation", Dependencies.KotlinX.core)
        add("implementation", Dependencies.Koin.android)

        add("testImplementation", Dependencies.JUnit)
        add("testImplementation", Dependencies.KotlinX.test)
        add("testImplementation", Dependencies.Truth.truth)
        add("testImplementation", Dependencies.Truth.java8)
    }
}

