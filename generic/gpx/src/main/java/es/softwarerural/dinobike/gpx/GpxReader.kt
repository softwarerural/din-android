package es.softwarerural.dinobike.gpx

import io.ticofab.androidgpxparser.parser.GPXParser
import io.ticofab.androidgpxparser.parser.domain.Gpx
import java.net.URL
import kotlin.coroutines.suspendCoroutine


class GpxReader {
    suspend fun read(gpxUrl: String) = suspendCoroutine<List<Point>> { continuation ->
        val gpx = GPXParser().parse(URL(gpxUrl).openStream())
        val points = when {
            gpx.routes.isNotEmpty() -> { mapPointsFromRoutes(gpx) }
            gpx.tracks.isNotEmpty() -> { mapPointsFromTracks(gpx) }
            else -> { emptyList() }
        }
        continuation.resumeWith(Result.success(points))
    }

    private fun mapPointsFromRoutes(gpx: Gpx) =
        gpx.routes.map { route ->
            route.routePoints.map {
                Point(
                    it.latitude,
                    it.longitude,
                    it.elevation
                )
            }
        }.flatten()

    private fun mapPointsFromTracks(gpx: Gpx) =
        gpx.tracks.map { track ->
            track.trackSegments.map { segment ->
                segment.trackPoints.map {
                    Point(
                        it.latitude,
                        it.longitude,
                        it.elevation
                    )
                }
            }.flatten()
        }.flatten()
}