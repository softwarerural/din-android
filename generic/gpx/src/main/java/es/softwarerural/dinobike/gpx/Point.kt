package es.softwarerural.dinobike.gpx

data class Point (
    val latitude: Double,
    val longitude: Double,
    val altitude: Double
)