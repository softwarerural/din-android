import es.softwarerural.dinobike.Dependencies
import es.softwarerural.dinobike.configureCompose

plugins {
    id("es.softwarerural.dinobike.android.library")
}

configureCompose()

dependencies {
    implementation(Dependencies.AndroidX.Compose.runtime)
    implementation(Dependencies.AndroidX.Compose.liveData)
    implementation(Dependencies.AndroidX.Compose.foundation)

    api("com.google.android.libraries.maps:maps:3.1.0-beta")
    api("com.google.maps.android:maps-v3-ktx:2.2.0")
}