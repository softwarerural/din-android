package es.softwarerural.dinobike.mapview

import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.runtime.Composable
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.compose.ui.viewinterop.AndroidView
import com.google.android.libraries.maps.GoogleMap
import com.google.android.libraries.maps.MapView
import com.google.maps.android.ktx.awaitMap
import kotlinx.coroutines.launch

@Composable
fun MapViewContainer(
    map: MapView,
    showInformation: (GoogleMap) -> Unit = {}
) {
    val coroutineScope = rememberCoroutineScope()

    AndroidView({ map }, modifier = Modifier
        .fillMaxHeight()
        .fillMaxWidth()) { mapView ->
        coroutineScope.launch {
            val googleMap = mapView.awaitMap()
            showInformation(googleMap)
        }
    }
}