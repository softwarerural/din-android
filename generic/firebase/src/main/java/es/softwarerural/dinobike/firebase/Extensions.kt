package es.softwarerural.dinobike.firebase

import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.ValueEventListener
import com.google.firebase.storage.FirebaseStorage
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.channels.sendBlocking
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.callbackFlow
import kotlinx.coroutines.flow.map
import java.lang.IllegalStateException
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException
import kotlin.coroutines.suspendCoroutine

suspend fun FirebaseStorage.fetchUrl(storageUrl: String): String {
    return suspendCoroutine { cont ->
        if (storageUrl.isNotBlank()) {
            getReferenceFromUrl(storageUrl).downloadUrl.addOnCompleteListener {
                cont.resumeWith(kotlin.Result.success(it.result?.toString() ?: ""))
            }.addOnCanceledListener {
                cont.resume("")
            }.addOnFailureListener {
                cont.resumeWithException(it)
            }
        } else {
            cont.resume("")
        }
    }

}

fun DatabaseReference.valueEventFlow(): Flow<Result<DataSnapshot>> = callbackFlow {
    val valueEventListener = object : ValueEventListener {
        override fun onDataChange(snapshot: DataSnapshot): Unit =
            sendBlocking(Result.success(snapshot))

        override fun onCancelled(error: DatabaseError): Unit =
            sendBlocking(Result.failure<DataSnapshot>(error.toException()))
    }
    addValueEventListener(valueEventListener)
    awaitClose {
        removeEventListener(valueEventListener)
    }
}

inline fun <reified T> DatabaseReference.listFlow(): Flow<Result<List<Pair<String, T>>>> =
    valueEventFlow().map { result ->
        result.fold(
            onFailure = { Result.failure(it) },
            onSuccess = { dataSnapshot ->
                val list = dataSnapshot.children.mapNotNull { pair ->
                    val key = pair.key
                    val dto = pair.getValue(T::class.java)
                    if (key == null || dto == null) null
                    else Pair(key, dto)
                }

                Result.success(list)
            }
        )
    }

inline fun <reified T> DatabaseReference.valueFlow(): Flow<Result<Pair<String, T>>> =
    valueEventFlow().map { result ->
        result.fold(
            onFailure = { Result.failure(it) },
            onSuccess = { dataSnapshot ->
                val key = dataSnapshot.key
                val dto = dataSnapshot.getValue(T::class.java)
                if (key == null || dto == null) Result.failure(IllegalStateException())
                else Result.success(Pair(key, dto))
            }
        )
    }