package es.softwarerural.dinobike.services.domain.model

data class DinobikeService(
    val serviceId: String,
    val name: String,
    val description: String
)