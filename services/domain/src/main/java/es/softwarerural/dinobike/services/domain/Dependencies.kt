package es.softwarerural.dinobike.services.domain

import es.softwarerural.dinobike.services.domain.usecases.GetCategoryServicesUseCase
import es.softwarerural.dinobike.services.domain.usecases.GetServiceCategoriesUseCase
import es.softwarerural.dinobike.services.domain.usecases.GetServiceCategoryUseCase
import org.koin.core.module.Module
import org.koin.dsl.module

val useCaseModule : Module = module {
    factory { GetServiceCategoriesUseCase(get()) }
    factory { GetServiceCategoryUseCase(get()) }
    factory { GetCategoryServicesUseCase(get()) }

}