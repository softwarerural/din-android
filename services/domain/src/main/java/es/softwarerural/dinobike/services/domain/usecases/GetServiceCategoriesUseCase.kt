package es.softwarerural.dinobike.services.domain.usecases

import es.softwarerural.dinobike.services.domain.repository.ServicesRepository

class GetServiceCategoriesUseCase(
    private val servicesRepository: ServicesRepository
) {
    operator fun invoke() = servicesRepository.getServiceCategories()
}