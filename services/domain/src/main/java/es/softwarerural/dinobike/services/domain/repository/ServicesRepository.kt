package es.softwarerural.dinobike.services.domain.repository

import es.softwarerural.dinobike.services.domain.model.DinobikeService
import es.softwarerural.dinobike.services.domain.model.ServiceCategory
import kotlinx.coroutines.flow.Flow

interface ServicesRepository {
    fun getServiceCategories() : Flow<Result<List<ServiceCategory>>>
    fun getServiceCategory(categoryId: String) : Flow<Result<ServiceCategory>>
    fun getCategoryServices(categoryId: String) : Flow<Result<List<DinobikeService>>>
}