package es.softwarerural.dinobike.services.domain.usecases

import es.softwarerural.dinobike.services.domain.repository.ServicesRepository

class GetCategoryServicesUseCase(
    private val servicesRepository: ServicesRepository
) {
    operator fun invoke(categoryId: String) = servicesRepository.getCategoryServices(categoryId)
}