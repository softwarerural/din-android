package es.softwarerural.dinobike.services.domain.usecases

import es.softwarerural.dinobike.services.domain.repository.ServicesRepository

class GetServiceCategoryUseCase(
    private val servicesRepository: ServicesRepository
) {
    operator fun invoke(categoryId: String) = servicesRepository.getServiceCategory(categoryId)
}