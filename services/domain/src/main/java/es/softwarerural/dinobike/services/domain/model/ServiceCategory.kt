package es.softwarerural.dinobike.services.domain.model

data class ServiceCategory(
    val categoryId: String,
    val name: String,
    val description: String,
    val picture: String = ""
)