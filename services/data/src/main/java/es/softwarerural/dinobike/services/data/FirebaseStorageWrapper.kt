package es.softwarerural.dinobike.services.data

import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.ktx.storage
import es.softwarerural.dinobike.firebase.fetchUrl

interface FirebaseStorageWrapper {
    suspend fun fetchUrl(storageUrl: String) : String
}

internal class FirebaseStorageWrapperImpl(
    private val storage: FirebaseStorage = Firebase.storage("gs://dinobike.appspot.com")
) : FirebaseStorageWrapper {
    override suspend fun fetchUrl(storageUrl: String) = storage.fetchUrl(storageUrl)

}