package es.softwarerural.dinobike.services.data.model

data class ServiceCategoryDTO(
    val name: String = "",
    val description: String = "",
    val picture: String = "",
)