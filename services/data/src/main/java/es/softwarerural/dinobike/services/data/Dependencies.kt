package es.softwarerural.dinobike.services.data

import es.softwarerural.dinobike.services.domain.repository.ServicesRepository
import org.koin.core.module.Module
import org.koin.dsl.module

@Suppress("USELESS_CAST")
val repositoryModule : Module = module {
    single {
        FirebaseServicesRepository(get()) as ServicesRepository
    }
    single {
        FirebaseStorageWrapperImpl() as FirebaseStorageWrapper
    }
    factory { CategoryMapperImpl(get()) as CategoryMapper }
}