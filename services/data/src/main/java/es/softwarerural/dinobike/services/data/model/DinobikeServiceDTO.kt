package es.softwarerural.dinobike.services.data.model

data class DinobikeServiceDTO(
    val name: String = "",
    val description: String = ""
)