package es.softwarerural.dinobike.services.data

import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase
import es.softwarerural.dinobike.firebase.listFlow
import es.softwarerural.dinobike.firebase.valueFlow
import es.softwarerural.dinobike.services.data.model.DinobikeServiceDTO
import es.softwarerural.dinobike.services.data.model.ServiceCategoryDTO
import es.softwarerural.dinobike.services.domain.model.DinobikeService
import es.softwarerural.dinobike.services.domain.model.ServiceCategory
import es.softwarerural.dinobike.services.domain.repository.ServicesRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map

internal class FirebaseServicesRepository(
    private val categoryMapper: CategoryMapper,
    private val database: FirebaseDatabase =
        Firebase.database("https://dinobike-default-rtdb.europe-west1.firebasedatabase.app/")
) : ServicesRepository {

    companion object {
        const val SERVICE_CATEGORIES_REFERENCE = "services"
    }

    override fun getServiceCategories(): Flow<Result<List<ServiceCategory>>> {
        return database.getReference(SERVICE_CATEGORIES_REFERENCE).listFlow<ServiceCategoryDTO>()
            .map { dataSnapshot ->
                dataSnapshot.fold(
                    onFailure = { Result.failure(it) },
                    onSuccess = { list ->
                        val result = list.map { pair ->
                            val key = pair.first
                            val dto = pair.second
                            categoryMapper(key, dto)
                        }

                        Result.success(result)
                    }
                )
            }
    }

    override fun getServiceCategory(categoryId: String): Flow<Result<ServiceCategory>> {
        return database.getReference("$SERVICE_CATEGORIES_REFERENCE/$categoryId")
            .valueFlow<ServiceCategoryDTO>()
            .map { dataSnapshot ->
                dataSnapshot.fold(
                    onFailure = { Result.failure(it) },
                    onSuccess = { pair ->
                        val key = pair.first
                        val dto = pair.second
                        val category = categoryMapper(key, dto)
                        Result.success(category)
                    }
                )
            }
    }

    override fun getCategoryServices(categoryId: String): Flow<Result<List<DinobikeService>>> {
        return database.getReference("$SERVICE_CATEGORIES_REFERENCE/$categoryId/services")
            .listFlow<DinobikeServiceDTO>()
            .map { dataSnapshot ->
                dataSnapshot.fold(
                    onFailure = { Result.failure(it) },
                    onSuccess = { list ->
                        val result = list.map { pair ->
                            val key = pair.first
                            val dto = pair.second
                            DinobikeService(
                                key,
                                dto.name,
                                dto.description
                            )
                        }

                        Result.success(result)
                    }
                )
            }
    }

}