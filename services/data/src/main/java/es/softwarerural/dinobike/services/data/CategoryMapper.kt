package es.softwarerural.dinobike.services.data

import es.softwarerural.dinobike.services.data.model.ServiceCategoryDTO
import es.softwarerural.dinobike.services.domain.model.ServiceCategory

internal interface CategoryMapper {
    suspend operator fun invoke(key: String, origin: ServiceCategoryDTO): ServiceCategory
}

internal class CategoryMapperImpl(
    private val storage: FirebaseStorageWrapper
) : CategoryMapper {
    override suspend fun invoke(key: String, origin: ServiceCategoryDTO): ServiceCategory {
        return ServiceCategory(
            categoryId = key,
            name = origin.name,
            description = origin.description,
            picture = storage.fetchUrl(origin.picture)
        )
    }
}