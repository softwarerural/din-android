-dontobfuscate

-keepattributes Signature

-keepclassmembers class es.softwarerural.dinobike.services.data.model.** {
  *;
}