plugins {
    id("es.softwarerural.dinobike.data")
}

dependencies {
    implementation(project(path = ":services:domain", configuration = "default"))
    implementation(project(path = ":generic:firebase"))

    implementation(platform("com.google.firebase:firebase-bom:27.1.0"))
    implementation("com.google.firebase:firebase-database-ktx")
    implementation("com.google.firebase:firebase-storage-ktx")
}