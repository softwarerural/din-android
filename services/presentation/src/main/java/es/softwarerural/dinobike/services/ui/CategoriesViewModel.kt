package es.softwarerural.dinobike.services.ui

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import es.softwarerural.dinobike.services.domain.model.ServiceCategory
import es.softwarerural.dinobike.services.domain.usecases.GetServiceCategoriesUseCase
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach

class CategoriesViewModel(
    private val getCategories: GetServiceCategoriesUseCase
) : ViewModel() {

    var state: CategoriesState by mutableStateOf(CategoriesState.Loading)
        private set

    init {
        loadCategories()
    }

    fun retry() {
        loadCategories()
    }

    private fun loadCategories() {
        getCategories().onEach { result ->
            state = result.fold(
                onSuccess = this@CategoriesViewModel::processSuccessfulState,
                onFailure = { CategoriesState.Error }
            )
        }.launchIn(viewModelScope)
    }

    private fun processSuccessfulState(result: List<ServiceCategory>) =
        if (result.isEmpty()) {
            CategoriesState.Empty
        } else {
            CategoriesState.Data(result)
        }
}