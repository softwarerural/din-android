package es.softwarerural.dinobike.services.ui.category

import es.softwarerural.dinobike.services.domain.model.ServiceCategory

sealed class CategoryState {
    object Loading : CategoryState()
    object Error : CategoryState()
    data class Data(val category: ServiceCategory) : CategoryState()
}