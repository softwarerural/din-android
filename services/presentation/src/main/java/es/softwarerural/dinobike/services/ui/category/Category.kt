package es.softwarerural.dinobike.services.ui.category

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.height
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import es.softwarerural.dinobike.core.presentation.getViewModel
import es.softwarerural.dinobike.ds.DinobikeTheme
import es.softwarerural.dinobike.ds.lce.LceError
import es.softwarerural.dinobike.ds.lce.LceLoading
import es.softwarerural.dinobike.ds.molecules.images.DetailPicture
import es.softwarerural.dinobike.ds.navigation.BackTopAppBar
import es.softwarerural.dinobike.services.domain.model.ServiceCategory
import org.koin.core.parameter.parametersOf

@Composable
internal fun Category(
    categoryId: String,
    navController: NavController
) {
    val viewModel: CategoryViewModel = getViewModel { parametersOf(categoryId) }
    LceCategory(
        categoryState = viewModel.state,
        services = { Services(categoryId = categoryId) },
        retry = viewModel::retry
    ) { navController.popBackStack() }
}

@Composable
internal fun LceCategory(
    categoryState: CategoryState,
    services: @Composable (String) -> Unit = {},
    retry: () -> Unit = {},
    upPress: () -> Unit = {}
) {
    Column {
        BackTopAppBar(
            title = processTitle(categoryState),
            upPress = { upPress() }
        )
        when (categoryState) {
            is CategoryState.Data -> {
                Category(categoryState.category, services)
            }
            is CategoryState.Error -> LceError(retry = { retry() })
            is CategoryState.Loading -> LceLoading()
        }
    }
}

@Composable
private fun Category(
    category: ServiceCategory,
    services: @Composable (String) -> Unit
) {
    Column {
        DetailPicture(
            pictureUrl = category.picture,
            contentDescription = category.name
        )
        Spacer(modifier = Modifier.height(8.dp))
        Text(category.description)
        services(category.categoryId)
    }
}

private fun processTitle(categoryState: CategoryState) =
    if (categoryState is CategoryState.Data) {
        categoryState.category.name
    } else {
        ""
    }

@Preview
@Composable
private fun LoadingPreview() {
    DinobikeTheme {
        LceCategory(categoryState = CategoryState.Loading)
    }
}

@Preview
@Composable
private fun ErrorPreview() {
    DinobikeTheme {
        LceCategory(categoryState = CategoryState.Error)
    }
}

//@Preview
//@Composable
//private fun DataPreview() {
//    DinobikeTheme {
//        LceCategory(
//            categoryState = CategoryState.Data(fakeServiceCategories[0]),
//            services = {
//                LceServices(CategoryServicesState.Loading)
//            }
//        )
//    }
//}