package es.softwarerural.dinobike.services.ui

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.material.Card
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import es.softwarerural.dinobike.ds.DinobikeTheme
import es.softwarerural.dinobike.ds.molecules.images.DetailPicture
import es.softwarerural.dinobike.ds.text.CardHeader
import es.softwarerural.dinobike.services.domain.model.ServiceCategory

@Composable
fun CategoriesList(
    items: List<ServiceCategory> = listOf(),
    goToCategory: (ServiceCategory) -> Unit = {}
) {
    val scrollState = rememberLazyListState()

    LazyColumn(
        contentPadding = PaddingValues(top = 8.dp),
        state = scrollState
    ) {
        items(items = items) { category ->
            CategoryItem(
                item = category,
                goToCategory = goToCategory
            )
        }
    }
}

@Composable
fun CategoryItem(
    item: ServiceCategory,
    goToCategory: (ServiceCategory) -> Unit = {}
) {
    Card(
        modifier = Modifier
            .fillMaxWidth()
            .padding(8.dp)
            .clickable { goToCategory(item) },
        elevation = 8.dp
    ) {
        Column {
            DetailPicture(
                pictureUrl = item.picture,
                contentDescription = item.name
            )
            CardHeader(item.name)
            Spacer(modifier = Modifier.height(8.dp))
        }
    }
}

//@Preview
//@Composable
//fun CategoryItemPreview() {
//    DinobikeTheme {
//        CategoryItem(fakeServiceCategories[0])
//    }
//}
//
//@Preview
//@Composable
//fun CategoriesListPreview() {
//    DinobikeTheme {
//        CategoriesList(fakeServiceCategories)
//    }
//}