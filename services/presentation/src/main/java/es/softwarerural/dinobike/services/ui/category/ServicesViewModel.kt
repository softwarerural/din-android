package es.softwarerural.dinobike.services.ui.category

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import es.softwarerural.dinobike.services.domain.model.DinobikeService
import es.softwarerural.dinobike.services.domain.usecases.GetCategoryServicesUseCase
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach

class ServicesViewModel(
    private val categoryId: String,
    private val getServices: GetCategoryServicesUseCase
) : ViewModel() {

    var state: CategoryServicesState by mutableStateOf(CategoryServicesState.Loading)
        private set

    init {
        getServices(categoryId).onEach { result ->
            state = result.fold(
                onSuccess = this@ServicesViewModel::processSuccessfulState,
                onFailure = { CategoryServicesState.Error }
            )
        }.launchIn(viewModelScope)
    }

    private fun processSuccessfulState(result: List<DinobikeService>) =
        if (result.isEmpty()) {
            CategoryServicesState.Empty
        } else {
            CategoryServicesState.Data(result)
        }
}