package es.softwarerural.dinobike.services.ui.category

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import es.softwarerural.dinobike.core.presentation.getViewModel
import es.softwarerural.dinobike.ds.DinobikeTheme
import es.softwarerural.dinobike.ds.text.CardHeader
import es.softwarerural.dinobike.services.domain.model.DinobikeService
import org.koin.core.parameter.parametersOf

@Composable
fun Services(categoryId: String) {
    val viewModel: ServicesViewModel = getViewModel { parametersOf(categoryId) }
    LceServices(viewModel.state)
}

@Composable
fun LceServices(
    categoryServicesState: CategoryServicesState
) {
    when (categoryServicesState) {
        is CategoryServicesState.Data -> {
            Column {
                CardHeader(content = "Servicios")
                Divider(
                    Modifier
                        .padding(horizontal = 8.dp)
                )
                LazyColumn(
                    modifier = Modifier
                        .fillMaxSize()
                        .padding(horizontal = 8.dp)
                ) {
                    items(items = categoryServicesState.data) { service ->
                        Service(service)
                    }
                }
            }

        }
        is CategoryServicesState.Loading -> {
            CircularProgressIndicator(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(8.dp)
            )
        }
        else -> {
        }
    }
}

@OptIn(ExperimentalAnimationApi::class)
@Composable
private fun Service(service: DinobikeService) {
    var showDetails by remember { mutableStateOf(false) }
    Column {
        Row(
            modifier = Modifier.fillMaxWidth(),
            horizontalArrangement = Arrangement.SpaceBetween
        ) {
            Text(service.name, modifier = Modifier.padding(vertical = 8.dp))
            TextButton(onClick = { showDetails = !showDetails }) {
                Text(
                    text = if (showDetails) "MENOS" else "MÁS",
                    style = MaterialTheme.typography.button
                )
            }
        }
        AnimatedVisibility(visible = showDetails) {
            Text(service.description, modifier = Modifier.padding(vertical = 8.dp))
        }
        Divider()
    }
}
//
//@Preview
//@Composable
//private fun ServicePreview() {
//    DinobikeTheme {
//        Service(fakeBikeServices[0])
//    }
//}
//
//@Preview
//@Composable
//private fun LoadingPreview() {
//    DinobikeTheme {
//        LceCategory(
//            categoryState = CategoryState.Data(fakeServiceCategories[0]),
//            services = {
//                LceServices(CategoryServicesState.Loading)
//            }
//        )
//    }
//}
//
//@Preview
//@Composable
//private fun ErrorPreview() {
//    DinobikeTheme {
//        LceCategory(
//            categoryState = CategoryState.Data(fakeServiceCategories[0]),
//            services = {
//                LceServices(CategoryServicesState.Error)
//            }
//        )
//    }
//}
//
//@Preview
//@Composable
//private fun EmptyPreview() {
//    DinobikeTheme {
//        LceCategory(
//            categoryState = CategoryState.Data(fakeServiceCategories[0]),
//            services = {
//                LceServices(CategoryServicesState.Empty)
//            }
//        )
//    }
//}
//
//@Preview
//@Composable
//private fun DataPreview() {
//    DinobikeTheme {
//        LceCategory(
//            categoryState = CategoryState.Data(fakeServiceCategories[0]),
//            services = {
//                LceServices(CategoryServicesState.Data(fakeBikeServices))
//            }
//        )
//    }
//}