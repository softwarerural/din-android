package es.softwarerural.dinobike.services.ui

import es.softwarerural.dinobike.services.data.repositoryModule
import es.softwarerural.dinobike.services.domain.useCaseModule
import es.softwarerural.dinobike.services.ui.category.CategoryViewModel
import es.softwarerural.dinobike.services.ui.category.ServicesViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.core.context.loadKoinModules
import org.koin.core.module.Module
import org.koin.dsl.module

private val viewModelModule: Module = module {
    viewModel { CategoriesViewModel(get()) }
    viewModel { parameters -> CategoryViewModel(categoryId = parameters.get(), get()) }
    viewModel { parameters -> ServicesViewModel(categoryId = parameters.get(), get()) }
}

object Services {
    fun init() = loadKoinModules(
        listOf(
            viewModelModule,
            useCaseModule,
            repositoryModule
        )
    )
}