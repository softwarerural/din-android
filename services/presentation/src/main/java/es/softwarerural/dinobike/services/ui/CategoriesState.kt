package es.softwarerural.dinobike.services.ui

import es.softwarerural.dinobike.services.domain.model.ServiceCategory

sealed class CategoriesState {
    object Loading : CategoriesState()
    object Empty : CategoriesState()
    object Error : CategoriesState()
    data class Data(val data: List<ServiceCategory>) : CategoriesState()
}