package es.softwarerural.dinobike.services.ui

import androidx.compose.runtime.Composable
import androidx.navigation.NavController
import androidx.navigation.NavGraphBuilder
import androidx.navigation.NavType
import androidx.navigation.compose.composable
import androidx.navigation.compose.navArgument
import androidx.navigation.compose.navigation
import es.softwarerural.dinobike.core.presentation.NavigationItem
import es.softwarerural.dinobike.services.ui.category.Category

internal const val SERVICES_ROUTE = "services"

val SERVICES_NAVIGATION_ROUTE = NavigationItem(
    SERVICES_ROUTE,
    R.string.services_route,
    R.drawable.ic_services
)

fun NavGraphBuilder.servicesNavigation(
    navController: NavController,
    bottomBar: @Composable () -> Unit = {}
) {
    navigation(
        startDestination = "$SERVICES_ROUTE.home",
        route = SERVICES_ROUTE
    ) {
        composable("$SERVICES_ROUTE.home") {
            Categories(navController, bottomBar)
        }
        composable(
            route = "$SERVICES_ROUTE.category/{categoryId}",
            arguments = listOf(navArgument("categoryId") { type = NavType.StringType })

        ) { backStackEntry ->
            val categoryId = backStackEntry.arguments?.getString("categoryId") ?: ""
            Category(categoryId, navController)
        }
    }
}