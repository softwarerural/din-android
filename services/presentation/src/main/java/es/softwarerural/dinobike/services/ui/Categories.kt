package es.softwarerural.dinobike.services.ui

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.navigation.NavController
import androidx.navigation.compose.navigate
import es.softwarerural.dinobike.core.presentation.getViewModel
import es.softwarerural.dinobike.ds.DinobikeTheme
import es.softwarerural.dinobike.ds.lce.LceEmpty
import es.softwarerural.dinobike.ds.lce.LceError
import es.softwarerural.dinobike.ds.lce.LceLoading
import es.softwarerural.dinobike.ds.preview.PreviewBottomBar
import es.softwarerural.dinobike.services.domain.model.ServiceCategory

@Composable
fun Categories(
    navController: NavController,
    bottomBar: @Composable () -> Unit = {}
) {
    val viewModel: CategoriesViewModel = getViewModel()
    StatelessCategories(
        state = viewModel.state,
        bottomBar = bottomBar,
        { item -> navController.navigate("$SERVICES_ROUTE.category/${item.categoryId}") },
        retry = viewModel::retry
    )
}

@Composable
fun StatelessCategories(
    state: CategoriesState,
    bottomBar: @Composable () -> Unit = {},
    goToCategory: (ServiceCategory) -> Unit = {},
    retry: () -> Unit = {}
) {
    Scaffold(
        topBar = {
            TopAppBar(
                title = {
                    Text(stringResource(id = R.string.services_route))
                }
            )
        },
        bottomBar = bottomBar
    ) { innerPadding ->
        Column(
            modifier = Modifier.padding(innerPadding)
        ) {
            when (state) {
                CategoriesState.Loading -> LceLoading()
                CategoriesState.Error -> LceError(retry = retry)
                CategoriesState.Empty -> LceEmpty(
                    R.string.empty_categories_list,
                    R.drawable.ic_services
                )
                is CategoriesState.Data -> CategoriesList(
                    state.data,
                    goToCategory
                )
            }
        }
    }
}

@Preview
@Composable
fun LoadingStateCategoriesPreview() {
    DinobikeTheme {
        StatelessCategories(
            CategoriesState.Loading,
            bottomBar = { PreviewBottomBar() }
        )
    }
}

@Preview
@Composable
fun EmptyStateCategoriesPreview() {
    DinobikeTheme {
        StatelessCategories(
            CategoriesState.Empty,
            bottomBar = { PreviewBottomBar() }
        )
    }
}

@Preview
@Composable
fun ErrorStateCategoriesPreview() {
    DinobikeTheme {
        StatelessCategories(
            CategoriesState.Error,
            bottomBar = { PreviewBottomBar() }
        )
    }
}

//@Preview
//@Composable
//fun DataStateCategoriesPreview() {
//    DinobikeTheme {
//        StatelessCategories(
//            CategoriesState.Data(fakeServiceCategories),
//            bottomBar = { PreviewBottomBar() }
//        )
//
//    }
//}