package es.softwarerural.dinobike.services.ui.category

import es.softwarerural.dinobike.services.domain.model.DinobikeService

sealed class CategoryServicesState {
    object Loading : CategoryServicesState()
    object Empty : CategoryServicesState()
    object Error : CategoryServicesState()
    data class Data(val data: List<DinobikeService>) : CategoryServicesState()
}