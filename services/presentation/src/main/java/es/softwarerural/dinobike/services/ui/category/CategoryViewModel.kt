package es.softwarerural.dinobike.services.ui.category

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import es.softwarerural.dinobike.services.domain.usecases.GetServiceCategoryUseCase
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach

class CategoryViewModel(
    private val categoryId: String,
    private val getCategory: GetServiceCategoryUseCase
) : ViewModel() {

    var state: CategoryState by mutableStateOf(CategoryState.Loading)
        private set

    init {
        loadCategory()
    }

    fun retry() {
        loadCategory()
    }

    private fun loadCategory() {
        getCategory(categoryId).onEach { result ->
            state = result.fold(
                onSuccess = { CategoryState.Data(it) },
                onFailure = { CategoryState.Error }
            )
        }.launchIn(viewModelScope)
    }
}