plugins {
    id("es.softwarerural.dinobike.presentation")
}

dependencies {
    implementation(project(path = ":services:domain", configuration = "default"))
    implementation(project(path = ":services:data"))
}